# TODO : Create the lambda role with aws_iam_role
resource "aws_iam_role" "iam_for_lambda" {
  name = "iam_for_lambda"
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

data "archive_file" "init" {
  type        = "zip"
  output_path = "empty_lambda_code.zip"
  source {
    content = "hello world"
    filename = "toto.txt"
  }
}

# TODO : Create lambda function with aws_lambda_function
resource "aws_lambda_function" "lambda_function" {
  function_name = "lambda_function_name"
  role          = aws_iam_role.iam_for_lambda.arn
  filename      = data.archive_file.init.output_path
  handler       = "lambda_main_app.lambda_handler"
  runtime       = "python3.7"
}

# TODO : Create a aws_iam_policy for the logging, this resource policy will be attached to the lambda role
resource "aws_iam_policy" "lambda_logging" {
  name        = "lambda_logging"
  path        = "/"
  description = "IAM policy for logging from a lambda"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "logs:CreateLogGroup",
        "logs:CreateLogStream",
        "logs:PutLogEvents"
      ],
      "Resource": "arn:aws:logs:*:*:*",
      "Effect": "Allow"
    }
  ]
}
EOF
}
# TODO : Attach the logging policy to the lamda role with aws_iam_role_policy_attachment
resource "aws_iam_role_policy_attachment" "aws_lambda_log_policy_attachment" {
  role       = aws_iam_role.iam_for_lambda.name
  policy_arn = aws_iam_policy.lambda_logging.arn
}

# TODO : Attach the AmazonS3FullAccess policy to the lambda role with aws_iam_role_policy_attachment
resource "aws_iam_role_policy_attachment" "aws_lambda_role_s3_policy_attachment" {
  role       = aws_iam_role.iam_for_lambda.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonS3FullAccess"
}
# TODO : Allow the lambda to be triggered by a s3 event with aws_lambda_permission
resource "aws_lambda_permission" "allow_bucket" {
  statement_id  = "AllowExecutionFromS3Bucket"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.lambda_function.function_name
  principal     = "s3.amazonaws.com"
  source_arn    = aws_s3_bucket.s3_job_offer_bucket.arn
}
