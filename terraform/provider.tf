# PROVIDER

provider "aws" {
  region  = var.region
  version = "~> 3.9"
}

terraform {
  required_version = "= 0.13.4"
}

terraform {
  required_providers {
    archive = {
      source = "hashicorp/archive"
      version = "2.0.0"
    }
  }
}

provider "archive" {
  # Configuration options
}